- [Block Cloudflare MITM Attack](../subfiles/about.bcma.md)
- [Are links vulnerable to MITM attack?](../subfiles/about.ismm.md)
- [Will these links block Tor user?](../subfiles/about.isat.md)
- [Which website rejected me?](../subfiles/about.urjm.md)


-----

### Aldonaĵo por Firefox/Chromium/Edge


- Funkcias plej bone kun plej nova [Tor Browser](https://www.torproject.org/download/)(rekomendas) aŭ [Mozilla Firefox _ESR_](https://portableapps.com/apps/internet/firefox-portable-esr)
- Permesilo: [MIT](../LICENSE.md)


-----

### Addons for Firefox/Chromium/Edge


- Works best with latest [Tor Browser](https://www.torproject.org/download/)(recommend) or [Mozilla Firefox _ESR_](https://portableapps.com/apps/internet/firefox-portable-esr)
- License: [MIT](../LICENSE.md)
