# What you can do to resist Cloudflare?

| 🖼 | 🖼 | 🖼 |
| --- | --- | --- |
| ![](../image/matthew_prince_teen.jpg) | ![](../image/matthew_prince.jpg) | ![](../image/blockedbymatthewprince.jpg) |


[Matthew Browning Prince (Twitter @eastdakota)](https://twitter.com/eastdakota), born on November 13th 1974, is the CEO and co-founder of CloudFlare.

Thanks to his rich dad, [John B. Prince](http://web.archive.org/web/20081002173414/http://www.mufranchisee.com/article/453/), he attended the [University of Chicago Law School](https://en.wikipedia.org/wiki/University_of_Chicago_Law_School) ('00) and [Harvard Business School](https://en.wikipedia.org/wiki/Harvard_Business_School) ('09). Prince taught Internet law and was a specialist in anti-spam laws and phishing investigations.


"*I’d suggest this was armchair analysis by kids – it’s hard to take seriously.*" [t](https://www.theguardian.com/technology/2015/nov/19/cloudflare-accused-by-anonymous-helping-isis)

"*That was simply unfounded paranoia, pretty big difference.*"  [t](https://twitter.com/xxdesmus/status/992757936123359233)

"*We also work with Interpol and other non-US entities*" [t](https://twitter.com/eastdakota/status/1203028504184360960)

"*Watching hacker skids on Github squabble about trying to bypass Cloudflare's new anti-bot systems continues to be my daily amusement.* 🍿" [t](https://twitter.com/eastdakota/status/1273277839102656515)


![](../image/whoismp.jpg)

---


<details>
<summary>click me

## Website consumer
</summary>


- If the website you like is using Cloudflare, tell them not to use Cloudflare.
  - Whining on social media such as Facebook, Reddit, Twitter or Mastodon makes no difference. [Actions are louder than hashtags.](https://twitter.com/phyzonloop/status/1274132092490862594)
  - Try to contact to the website owner if you want to make yourself useful.

[Cloudflare said](https://github.com/Eloston/ungoogled-chromium/issues/783):
```
We recommend that you reach out to the administrators for the specific services or sites that you run into issue with and share your experience.
```

[If you don't ask for it, website owner never know this problem.](../PEOPLE.md)

![](../image/liberapay.jpg)

[Successful example](https://counterpartytalk.org/t/turn-off-cloudflare-on-counterparty-co-plz/164/5).<br>
You have a problem? [Raise your voice now.](https://github.com/maraoz/maraoz.github.io/issues/1) Example below.

```
You are just helping corporate censorship and mass surveillance.
https://codeberg.org/crimeflare/cloudflare-tor/src/branch/master/README.md
```

```
Your web page is in the privacy-abusing private walled-garden of CloudFlare.
https://codeberg.org/crimeflare/cloudflare-tor/
```

- Take some time to read website's privacy policy.
  - if the website is behind Cloudflare or website is using services connected to Cloudflare.

It must explain what the "Cloudflare" is, and ask for permission to share your data with Cloudflare. Failure to do so will result in the breach of trust and the website in question should be avoided.

[An acceptable privacy policy example is here](https://archive.is/bDlTz) ("Subprocessors" > "Entity Name")

```
I've read your privacy policy and I cannot find the word Cloudflare.
I refuse to share data with you if you continue to feed my data to Cloudflare.
https://codeberg.org/crimeflare/cloudflare-tor/
```

This is an example of privacy policy which does not have the word Cloudflare.
[Liberland Jobs](https://archive.is/daKIr) [privacy policy](https://docsend.com/view/feiwyte):

![](../image/cfwontobey.jpg)

Cloudflare have their own privacy policy.
[Cloudflare loves doxxing people.](https://www.reddit.com/r/GamerGhazi/comments/2s64fe/be_wary_reporting_to_cloudflare/)

Here's a good example for website's signup form.
AFAIK, zero website do this. Will you trust them?

```
By clicking “Sign up for XYZ”, you agree to our terms of service and privacy statement.
You also agree to share your data with Cloudflare and also agrees to cloudflare's privacy statement.
If Cloudflare leak your information or won't let you to connect to our servers, it's not our fault. [*]

[ Sign up ] [ I disagree ]
```
[*] [PEOPLE.md](../PEOPLE.md)


- Try not to use their service. Remember you are being watched by Cloudflare.
  - ["I'm in your TLS, sniffin' your passworz"](../image/iminurtls.jpg)

- Search for other website. There are alternatives and opportunites on the internet!

- Convince your friends to use Tor on the daily basis.
  - Anonymity should be the standard of the open internet!
  - [Do note that the Tor project dislikes this project.](../HISTORY.md)

</details>

------

<details>
<summary>click me

## Add-ons
</summary>

- If your browser is Firefox, Tor Browser, or Ungoogled Chromium use one of these add-ons below.
  - If you want to add other new add-on ask about it first.


| Name | Developer | Support | Can Block | Can Notify | Chrome |
| -------- | -------- | -------- | -------- | -------- | -------- |
| [Bloku Cloudflaron MITM-Atakon](../subfiles/about.bcma.md) | #Addon | [ ? ](README.md) | **Yes**     | **Yes**     |  **Yes** |
| [Ĉu ligoj estas vundeblaj al MITM-atako?](../subfiles/about.ismm.md) | #Addon | [ ? ](README.md) | No     | **Yes**     |  **Yes** |
| [Ĉu ĉi tiuj ligoj blokos Tor-uzanton?](../subfiles/about.isat.md) | #Addon | [ ? ](README.md) | No     | **Yes**     |  **Yes** |
| [Block Cloudflare MITM Attack](https://trac.torproject.org/projects/tor/attachment/ticket/24351/block_cloudflare_mitm_attack-1.0.14.1-an%2Bfx.xpi)<br>[**DELETED BY TOR PROJECT**](../HISTORY.md) | nullius | [ ? ](tool/block_cloudflare_mitm_fx), [Link](README.md) | **Yes**     | **Yes**     |  No |
| [TPRB](http://34ahehcli3epmhbu2wbl6kw6zdfl74iyc4vg3ja4xwhhst332z3knkyd.onion/) | Sw | [ ? ](http://34ahehcli3epmhbu2wbl6kw6zdfl74iyc4vg3ja4xwhhst332z3knkyd.onion/) | **Yes**     | **Yes**     |  No |
| [Detect Cloudflare](https://addons.mozilla.org/en-US/firefox/addon/detect-cloudflare/) | Frank Otto | [ ? ](https://github.com/traktofon/cf-detect) | No     | **Yes**     |  No |
| [True Sight](https://addons.mozilla.org/en-US/firefox/addon/detect-cloudflare-plus/) | claustromaniac | [ ? ](https://github.com/claustromaniac/detect-cloudflare-plus) | No     | **Yes**     |  No |
| [Which Cloudflare datacenter am I visiting?](https://addons.mozilla.org/en-US/firefox/addon/cf-pop/) | 依云 | [ ? ](https://github.com/lilydjwg/cf-pop) | No     | **Yes**     |  No |


- "Decentraleyes" can stop connection to "CDNJS (Cloudflare)".
  - It prevents a lot of requests from reaching networks, and serves local files to keep sites from breaking.
  - The developer replied: "[very concerning indeed](https://github.com/Synzvato/decentraleyes/issues/236#issuecomment-352049501)", "[widespread usage severely centralizes the web](https://github.com/Synzvato/decentraleyes/issues/251#issuecomment-366752049)"

- [You can also remove or distrust Cloudflare certificate from your Certificate Authority(CA).](https://www.ssl.com/how-to/remove-root-certificate-firefox/)

</details>

------

<details>
<summary>click me

## Website owner / Web developer
</summary>


![](../image/word_cloudflarefree.jpg)

- Do not use Cloudflare solution, Period.
  - You can do better than that, right? [Here's how to remove Cloudflare subscriptions, plans, domains, or accounts.](https://support.cloudflare.com/hc/en-us/articles/200167776-Removing-subscriptions-plans-domains-or-accounts)

| 🖼 | 🖼 |
| --- | --- |
| ![](../image/htmlalertcloudflare.jpg) | ![](../image/htmlalertcloudflare2.jpg) |

- Want more customers? You know what to do. Hint is "above line".
  - [Hello, you wrote "We take your privacy seriously" but I got "Error 403 Forbidden Anonymous Proxy Not Allowed".](https://it.slashdot.org/story/19/02/19/0033255/stop-saying-we-take-your-privacy-and-security-seriously) Why are you blocking Tor Or VPN? [And why are you blocking temporary emails?](http://nomdjgwjvyvlvmkolbyp3rocn2ld7fnlidlt2jjyotn3qqsvzs2gmuyd.onion/mail/)

![](../image/anonexist.jpg)

- Using Cloudflare will increase chances of an outage. Visitors can't access to your website if your server is down or Cloudflare is down.
  - [Did you really think Cloudflare never go down?](https://www.ibtimes.com/cloudflare-down-not-working-sites-producing-504-gateway-timeout-errors-2618008) [Another](https://twitter.com/Jedduff/status/1097875615997399040) [sample](https://twitter.com/search?f=tweets&vertical=default&q=Cloudflare%20is%20having%20problems). [Need more](../PEOPLE.md)?

![](../image/cloudflareinternalerror.jpg)

- Using Cloudflare to proxy your "API service", "software update server" or "RSS feed" will harm your customer. A customer called you and said "I can't use your API anymore", and you have no idea what is going on. Cloudflare can silently block your customer. Do you think it is okay?
  - There are many RSS reader client and RSS reader online service. Why are you publishing RSS feed if you're not allowing people to subscribe?

![](../image/rssfeedovercf.jpg)

- Do you need HTTPS certificate? Use "Let's Encrypt" or just buy it from CA company.

- Do you need DNS server? Can't set up your own server? How about them: [Hurricane Electric Free DNS](https://dns.he.net/), [Dyn.com](https://dyn.com/dns/), [1984 Hosting](https://www.1984hosting.com/), [Afraid.Org (Admin delete your account if you use TOR)](https://freedns.afraid.org/)

- Looking for hosting service? Free only? How about them: [Onion Service](http://vww6ybal4bd7szmgncyruucpgfkqahzddi37ktceo3ah7ngmcopnpyyd.onion/en/security/network-security/tor/onionservices-best-practices), [Free Web Hosting Area](https://freewha.com/), [Autistici/Inventati Web Site Hosting](https://www.autinv5q6en4gpf4.onion/services/website), [Github Pages](https://pages.github.com/), [Surge](https://surge.sh/)
  - [Alternatives to Cloudflare](../subfiles/cloudflare-alternatives.md)

- Are you using "cloudflare-ipfs.com"? [Do you know Cloudflare IPFS is bad?](../PEOPLE.md)

- Install Web Application Firewall such as OWASP and Fail2Ban on your server and configure it properly.
  - Blocking Tor is not a solution. Don't punish everyone just for small bad users.

- Redirect or block "Cloudflare Warp" users from accessing your website. And provide a reason if you can.

> IP list: "[Cloudflare’s current IP ranges](cloudflare_inc/)"

> A: Just block them

```
server {
...
deny 173.245.48.0/20;
deny 103.21.244.0/22;
deny 103.22.200.0/22;
deny 103.31.4.0/22;
deny 141.101.64.0/18;
deny 108.162.192.0/18;
deny 190.93.240.0/20;
deny 188.114.96.0/20;
deny 197.234.240.0/22;
deny 198.41.128.0/17;
deny 162.158.0.0/15;
deny 104.16.0.0/12;
deny 172.64.0.0/13;
deny 131.0.72.0/22;
deny 2400:cb00::/32;
deny 2606:4700::/32;
deny 2803:f800::/32;
deny 2405:b500::/32;
deny 2405:8100::/32;
deny 2a06:98c0::/29;
deny 2c0f:f248::/32;
...
}
```

> B: Redirect to warning page

```
http {
...
geo $iscf {
default 0;
173.245.48.0/20 1;
103.21.244.0/22 1;
103.22.200.0/22 1;
103.31.4.0/22 1;
141.101.64.0/18 1;
108.162.192.0/18 1;
190.93.240.0/20 1;
188.114.96.0/20 1;
197.234.240.0/22 1;
198.41.128.0/17 1;
162.158.0.0/15 1;
104.16.0.0/12 1;
172.64.0.0/13 1;
131.0.72.0/22 1;
2400:cb00::/32 1;
2606:4700::/32 1;
2803:f800::/32 1;
2405:b500::/32 1;
2405:8100::/32 1;
2a06:98c0::/29 1;
2c0f:f248::/32 1;
}
...
}

server {
...
if ($iscf) {rewrite ^ https://example.com/cfwsorry.php;}
...
}

<?php
header('HTTP/1.1 406 Not Acceptable');
echo <<<CLOUDFLARED
Thank you for visiting ourwebsite.com!<br />
We are sorry, but we can't serve you because your connection is being intercepted by Cloudflare.<br />
Please read https://codeberg.org/crimeflare/cloudflare-tor for more information.<br />
CLOUDFLARED;
die();
```

- Set up Tor Onion Service or I2P insite if you believe in freedom and welcome anonymous users.

- Ask for advice from other Clearnet/Tor dual website operators and make anonymous friends!

</details>

------

<details>
<summary>click me

## Software user
</summary>


- Discord is using CloudFlare. Alternatives? We recommend [**Briar** (Android)](https://f-droid.org/en/packages/org.briarproject.briar.android/), [Ricochet (PC)](https://ricochet.im/), [Tox + Tor (Android/PC)](https://tox.chat/download.html)
  - Briar includes Tor daemon so you don't have to install Orbot.
  - Qwtch developers, Open Privacy, deleted stop_cloudflare project from their git service without notice.

- If you use Debian GNU/Linux, or any derivative, subscribe: [bug #831835](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835). And if you can, help verify the patch, and help the maintainer come to the right conclusion on whether it should be accepted.

- Always recommend these browsers.

| Name | Developer | Support | Comment |
| -------- | -------- | -------- | -------- |
| [Ungoogled-Chromium](https://ungoogled-software.github.io/ungoogled-chromium-binaries/) | Eloston | [ ? ](https://github.com/Eloston/ungoogled-chromium) | PC (Win, Mac, Linux)  _!Tor_ |
| [Bromite](https://www.bromite.org/fdroid) | Bromite | [ ? ](https://github.com/bromite/bromite/issues) | Android  _!Tor_ |
| [Tor Browser](https://www.torproject.org/download/) | Tor Project | [ ? ](https://support.torproject.org/) | PC (Win, Mac, Linux)  _Tor_|
| [Tor Browser Android](https://www.torproject.org/download/) | Tor Project | [ ? ](https://support.torproject.org/) | Android  _Tor_|
| [Onion Browser](https://itunes.apple.com/us/app/onion-browser/id519296448?mt=8) | Mike Tigas | [ ? ](https://github.com/OnionBrowser/OnionBrowser/issues) | Apple iOS  _Tor_|
| [GNU/Icecat](https://www.gnu.org/software/gnuzilla/) | GNU | [ ? ](https://www.gnu.org/software/gnuzilla/) | PC (Linux) |
| [IceCatMobile](https://f-droid.org/en/packages/org.gnu.icecat/) | GNU | [ ? ](https://lists.gnu.org/mailman/listinfo/bug-gnuzilla) | Android |
| [Iridium Browser](https://iridiumbrowser.de/about/) | Iridium | [ ? ](https://github.com/iridium-browser/iridium-browser/) | PC (Win, Mac, Linux, OpenBSD) |


Other software's privacy is imperfect. This doesn't mean Tor browser is "perfect".
There is no 100% secure nor 100% private on the internet and technology.

- Don't want to use Tor? You can use any browser with Tor daemon.
  - [Note that the Tor project don't like this.](https://support.torproject.org/tbb/tbb-9/) Use Tor Browser if you are able to do so.
- [How to use Chromium with Tor](../subfiles/chromium_tor.md)


Let's talk about other software's privacy.

- [If you really need to use Firefox, pick "Firefox ESR".](https://www.mozilla.org/en-US/firefox/organizations/)
  - [Firefox - Spyware Watchdog](https://spyware.neocities.org/articles/firefox.html)
  - [Firefox rejects free speech, bans free speech](https://web.archive.org/web/20200423010026/https://reclaimthenet.org/firefox-rejects-free-speech-bans-free-speech-commenting-plugin-dissenter-from-its-extensions-gallery/)
  - ["100+ downvotes. It seems like asking a software company to stick to... software is just too much these days."](https://old.reddit.com/r/firefox/comments/gutdiw/weve_got_work_to_do_the_mozilla_blog/fslbbb6/)
  - [Uh, why is Firefox showing me sponsored links in my URL bar?](https://www.reddit.com/r/firefox/comments/jybx2w/uh_why_is_firefox_showing_me_sponsored_links_in/)
  - [Mozilla - Devil Incarnate](https://digdeeper.neocities.org/ghost/mozilla.html)

- [Remember, Mozilla is using Cloudflare service.](https://www.robtex.com/dns-lookup/www.mozilla.org) [They're also using Cloudflare's DNS service on their product.](https://www.theregister.co.uk/2018/03/21/mozilla_testing_dns_encryption/)

- [Mozilla officially rejected this ticket.](https://bugzilla.mozilla.org/show_bug.cgi?id=1426618)

- [Firefox Focus is a joke.](https://github.com/mozilla-mobile/focus-android/issues/1743) [They promised to turn off telemetry but they changed it.](https://github.com/mozilla-mobile/focus-android/issues/4210)

- [PaleMoon/Basilisk developer loves Cloudflare.](https://github.com/mozilla-mobile/focus-android/issues/1743#issuecomment-345993097)
  - [Pale Moon's Archive Server hacked and spread malware for 18 Months](https://www.reddit.com/r/privacytoolsIO/comments/cc808y/pale_moons_archive_server_hacked_and_spread/)
  - He also hate Tor users - "[Let it be hostile towards Tor. I think most sites should be hostile towards Tor considering its extremely high abuse factor.](https://github.com/yacy/yacy_search_server/issues/314#issuecomment-565932097)"

- [Waterfox have severe "phones home" problem](https://spyware.neocities.org/articles/waterfox.html)

- [Google Chrome is a spyware.](https://www.gnu.org/proprietary/malware-google.en.html)
  - [Google profiles your activity.](https://spyware.neocities.org/articles/chrome.html)

- [SRWare Iron make too many phones home connection.](https://spyware.neocities.org/articles/iron.html) It also connect to google domains.

- [Brave Browser whitelist Facebook/Twitter trackers.](https://www.bleepingcomputer.com/news/security/facebook-twitter-trackers-whitelisted-by-brave-browser/)
  - [Here's more issues.](https://spyware.neocities.org/articles/brave.html)
  - [binance affiliate ID](https://twitter.com/cryptonator1337/status/1269594587716374528)

- [Microsoft Edge lets Facebook run Flash code behind users' backs.](https://www.zdnet.com/article/microsoft-edge-lets-facebook-run-flash-code-behind-users-backs/)

- [Vivaldi does not respect your privacy.](https://spyware.neocities.org/articles/vivaldi.html)

- [Opera spyware level: Extremely High](https://spyware.neocities.org/articles/opera.html)

- Apple iOS: [You shouldn't be using iOS at all, mainly because it is malware.](https://www.gnu.org/proprietary/malware-apple.html)

Therefore we recommend above table only. Nothing else.

</details>

------

<details>
<summary>click me

## Mozilla Firefox user
</summary>


- "Firefox Nightly" will send debug-level information to Mozilla servers without opt-out method.
  - [Mozilla servers are behing Cloudflare](https://www.digwebinterface.com/?hostnames=www.mozilla.org%0D%0Amozilla.cloudflare-dns.com&type=&ns=resolver&useresolver=8.8.4.4&nameservers=)

- It is possible to prohibit Firefox to connect to Mozilla servers.
  - [Mozilla's policy-templates guide](https://github.com/mozilla/policy-templates/blob/master/README.md)
  - Keep in mind this trick might stop working in later version because Mozilla likes to whitelist themselves.
  - Use firewall and DNS filter to block them completely.

"`/distribution/policies.json`"

>     "WebsiteFilter": {
> 		"Block": [
> 		"*://*.mozilla.com/*",
> 		"*://*.mozilla.net/*",
> 		"*://*.mozilla.org/*",
> 		"*://webcompat.com/*",
> 		"*://*.firefox.com/*",
> 		"*://*.thunderbird.net/*",
> 		"*://*.cloudflare.com/*"
> 		]
>     },


- ~~Report a bug on mozilla's tracker, telling them not to use Cloudflare.~~ There was a bug report on bugzilla. Many people were posted their concern, however the bug was hidden by the admin in 2018.

- You can disable DoH in Firefox.
  - [Change default DNS provider of firefox](../subfiles/change-firefox-dns.md)

![](../image/firefoxdns.jpg)

- [If you would like to use non-ISP DNS, consider using OpenNIC Tier2 DNS service  or any of non-Cloudflare DNS services.](https://wiki.opennic.org/start)
![](../image/opennic.jpg)
  - Block Cloudflare with DNS. [Crimeflare DNS](https://dns.crimeflare.eu.org/)

- You can use Tor as DNS resolver. [If you're not Tor expert, ask question here.](https://tor.stackexchange.com/)

> **How?**
> 1. Download Tor and install it on your computer.
> 2. Add this line to "torrc" file.
> DNSPort 127.0.0.1:53
> 3. Restart Tor.
> 4. Set your computer's DNS server to "127.0.0.1".

</details>

------

<details>
<summary>click me

## Action
</summary>


- Tell others around you about the dangers of Cloudflare.

- [Help improve this repository.](https://codeberg.org/crimeflare/cloudflare-tor).
  - Both the lists, the arguments against it and the details.

- [Document and make very public where things go wrong with Cloudflare (and similar companies), making sure to mention this repository when you do so](https://codeberg.org/crimeflare/cloudflare-tor) :)

- Get more people using Tor by default so they can experience the web from the perspective of different parts of the world.

- Start groups, in social media and meatspace, dedicated to liberating the world from Cloudflare.

- Where appropriate, link to these groups on this repository - this can be a place for coordinating working together as groups.

- [Start a coop that can provide a meaningful non corporate alternative to Cloudflare.](../subfiles/cloudflare-alternatives.md)

- Let us know of any alternatives to help at least provide multiple layered defence against Cloudflare.

- If you are a Cloudflare customer, set your privacy settings, and wait for them to violate them.
  - [Then bring them under anti-spam / privacy violation charges.](https://twitter.com/thexpaw/status/1108424723233419264)

- If you are in the United States of America and the website in question is a bank or an accountant, try to bring legal pressure under the Gramm–Leach–Bliley Act, or the Americans with DIsabilities Act and report back to us how far you get.

- If the website is a government site, try to bring legal pressure under the 1st Amendment of the US Constitution.

- If you are EU citizen, contact the website to send your personal information under the General Data Protection Regulation. If they refuse to give you your information, that's a violation of the law.

- For companies that claim to offer service on their website try reporting them as "false advertising" to consumer protection organizations and BBB. Cloudflare websites are served by Cloudflare servers.

- [The ITU suggest in the US context that Cloudflare is starting to get big enough that antitrust law might be brought down upon them.](https://www.itu.int/en/ITU-T/Workshops-and-Seminars/20181218/Documents/Geoff_Huston_Presentation.pdf)

- It's conceivable that the GNU GPL version 4 could include a provision against storing source code behind such a service, requiring for all GPLv4 and later programs that at least the source code is accessible via a medium that does not discriminate against Tor users.

</details>

------

### Comments

```
There is always hope in resistance.

Resistance is fertile.

Even some of the darker outcomes comes to be, the very act of resistance trains us to continue to destabilize the dystopic status quo that results.

Resist!
```

```
Someday, you'll understand why we wrote this.
```

```
There isn't anything futuristic about this. We have already lost.
```

### Now, what did you do today?


![](../image/stopcf.jpg)
