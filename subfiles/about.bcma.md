### Block Cloudflare MITM Attack

`Take action against Cloudflare`

![](../image/goodorbad.jpg)


```

This add-on will block, notify, or redirect your request if the target website is using Cloudflare.
Submit to global surveillance or resist. The choice is yours.
 
This add-on never send any data.
Your cloudflare-domain collection is yours.

```


- [Code](https://codeberg.org/crimeflare/cloudflare-tor/src/branch/master/addons/code/bcma)
- Download add-on
  - From Ss (_Recommend_): [Firefox ESR / Chromium / Edge](https://sercxi.nnpaefp7pkadbxxkhz2agtbv2a4g5sgo2fbmv3i7czaua354334uqqad.onion/)
  - From Gitea (Delay Sync): [FirefoxESR](https://codeberg.org/crimeflare/cloudflare-tor/raw/branch/master/addons/releases/bcma.xpi) / [Chromium / Edge](https://codeberg.org/crimeflare/cloudflare-tor/raw/branch/master/addons/releases/bcma.crx)
