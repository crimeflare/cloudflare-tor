### Which website rejected me?

`Your website rejected me, but I know I'm going to be okay.`

![](../image/aonurjm.jpg)


```

1. Your website reject me.
2. This add-on will log the FQDN, URL, Status code and current time.
3. You can review it from Option menu.
4. Yell at website owner with this proof. (optional)
 
This add-on never send any data.
Your domain collection is yours.

```


- [Code](https://codeberg.org/crimeflare/cloudflare-tor/src/branch/master/addons/code/ureject)
- Download add-on
  - From Ss (_Recommend_): [Firefox ESR / Chromium / Edge](https://sercxi.nnpaefp7pkadbxxkhz2agtbv2a4g5sgo2fbmv3i7czaua354334uqqad.onion/)
  - From Gitea (Delay Sync): [FirefoxESR](https://codeberg.org/crimeflare/cloudflare-tor/raw/branch/master/addons/releases/urjm.xpi) / [Chromium / Edge](https://codeberg.org/crimeflare/cloudflare-tor/raw/branch/master/addons/releases/urjm.crx)
