# Shared on Mastodon


- [About mastodonwch](../tool/mastodonwch/README.md)
- Shared on Mastodon
  - [Top 100 - Shared any links](shared_on_mastodon.md#top-100-shared-any-links)
  - [Top 100 - Shared Cloudflare links](shared_on_mastodon.md#top-100-shared-cloudflare-links)
  - [Top 100 - Shared FQDN](shared_on_mastodon.md#top-100-shared-fqdn)
  - [About Mastodon Users](shared_on_mastodon.md#about-mastodon-users)
  - [About Mastodon Servers](shared_on_mastodon.md#about-mastodon-servers)

----


- [View latest information](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/)
  - `Toots has links`: tweet has any link
  - `Toots has Cloudflare link`: tweet has cloudflare link
  - `Active Mastodon Servers`: has users who shared any link

[//]: # (do not edit this line start; t0)

| About |   |
| --- | --- |
| Toots has links | 961,003 |
| Toots has Cloudflare link | 155,518 (16.18%) |
| Active Mastodon Servers | 2,028 |


[//]: # (do not edit this line end)


### Top 100 - Shared any links
- [View latest information](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=a)
  - `Who`: Mastodon user
  - `Toots`: tweet has any link

[//]: # (do not edit this line start; t1)

| # | Who | Toots |
| --- | --- | --- |
| 1 | [newsbot@dajiaweibo.com](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=newsbot@dajiaweibo.com) | 24,316 |
| 2 | [google@societal.co](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=google@societal.co) | 22,131 |
| 3 | [nowplaying@toot-lab.reclaim.technology](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=nowplaying@toot-lab.reclaim.technology) | 11,457 |
| 4 | [amb_noticias@botsin.space](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=amb_noticias@botsin.space) | 9,223 |
| 5 | [UnitooWebRadio@botsin.space](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=UnitooWebRadio@botsin.space) | 8,415 |
| 6 | [tomohiro_amami@pawoo.net](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=tomohiro_amami@pawoo.net) | 7,932 |
| 7 | [metro@societal.co](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=metro@societal.co) | 6,309 |
| 8 | [prensa_maldita@friendica.chilemasto.casa](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=prensa_maldita@friendica.chilemasto.casa) | 6,248 |
| 9 | [incestfan@my.dirtyhobby.xyz](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=incestfan@my.dirtyhobby.xyz) | 6,088 |
| 10 | [thecliniccl@friendica.chilemasto.casa](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=thecliniccl@friendica.chilemasto.casa) | 5,293 |
| 11 | [IndianExpress@newsbots.eu](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=IndianExpress@newsbots.eu) | 4,826 |
| 12 | [WowMachineRadio@botsin.space](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=WowMachineRadio@botsin.space) | 4,709 |
| 13 | [itnewsbot@schleuss.online](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=itnewsbot@schleuss.online) | 4,286 |
| 14 | [archlinux@pokemon.men](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=archlinux@pokemon.men) | 4,077 |
| 15 | [cnbeta@pokemon.men](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=cnbeta@pokemon.men) | 3,987 |
| 16 | [reuters@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=reuters@mstdn.foxfam.club) | 3,921 |
| 17 | [NYPost@mastodon.cloud](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=NYPost@mastodon.cloud) | 3,792 |
| 18 | [thetribunechd@newsbots.eu](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=thetribunechd@newsbots.eu) | 3,778 |
| 19 | [hknewsbot@pawoo.net](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=hknewsbot@pawoo.net) | 3,710 |
| 20 | [newpom@pomdon.work](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=newpom@pomdon.work) | 3,618 |
| 21 | [nowthisnews@newsbots.eu](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=nowthisnews@newsbots.eu) | 3,519 |
| 22 | [patrikaidotcom@masthead.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=patrikaidotcom@masthead.social) | 3,405 |
| 23 | [business@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=business@mstdn.foxfam.club) | 3,348 |
| 24 | [trainkanto@mastodon.chotto.moe](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=trainkanto@mastodon.chotto.moe) | 3,297 |
| 25 | [hntooter@mastodon.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=hntooter@mastodon.social) | 3,127 |
| 26 | [hackernews@die-partei.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=hackernews@die-partei.social) | 3,109 |
| 27 | [spon@friendica.produnis.de](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=spon@friendica.produnis.de) | 3,093 |
| 28 | [covid_stats@mastodon.cloud](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=covid_stats@mastodon.cloud) | 3,002 |
| 29 | [elpuntavui@mastodont.cat](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=elpuntavui@mastodont.cat) | 2,995 |
| 30 | [Sanfermin@mastodon.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=Sanfermin@mastodon.social) | 2,813 |
| 31 | [dailynews@mastodon.in.th](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=dailynews@mastodon.in.th) | 2,799 |
| 32 | [HispaBot_Noticias@botsin.space](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=HispaBot_Noticias@botsin.space) | 2,789 |
| 33 | [tr@mhc.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=tr@mhc.social) | 2,711 |
| 34 | [BBCNews@newsbots.eu](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=BBCNews@newsbots.eu) | 2,689 |
| 35 | [svtnyheter@newsbots.eu](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=svtnyheter@newsbots.eu) | 2,676 |
| 36 | [NYPost@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=NYPost@mstdn.foxfam.club) | 2,643 |
| 37 | [chouti@9kb.me](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=chouti@9kb.me) | 2,593 |
| 38 | [gitcomteam@mastodon.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=gitcomteam@mastodon.social) | 2,537 |
| 39 | [RichardCharles@brighteon.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=RichardCharles@brighteon.social) | 2,495 |
| 40 | [Sputnikint@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=Sputnikint@mstdn.foxfam.club) | 2,483 |
| 41 | [VTVCANAL8@mastodon.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=VTVCANAL8@mastodon.social) | 2,469 |
| 42 | [MangaPostJapan@pawoo.net](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=MangaPostJapan@pawoo.net) | 2,421 |
| 43 | [flutter@pokemon.men](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=flutter@pokemon.men) | 2,395 |
| 44 | [corona_news@botsin.space](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=corona_news@botsin.space) | 2,394 |
| 45 | [traintouhoku@mastodon.chotto.moe](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=traintouhoku@mastodon.chotto.moe) | 2,349 |
| 46 | [el_ciudadano@friendica.chilemasto.casa](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=el_ciudadano@friendica.chilemasto.casa) | 2,227 |
| 47 | [dlf@loma.ml](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=dlf@loma.ml) | 2,219 |
| 48 | [cigger2@pawoo.net](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=cigger2@pawoo.net) | 2,210 |
| 49 | [actualidadrt@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=actualidadrt@mstdn.foxfam.club) | 2,200 |
| 50 | [lemonde@newsbots.eu](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=lemonde@newsbots.eu) | 2,158 |
| 51 | [johndolph@brighteon.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=johndolph@brighteon.social) | 2,155 |
| 52 | [hn50@social.lansky.name](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=hn50@social.lansky.name) | 2,126 |
| 53 | [reddit@societal.co](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=reddit@societal.co) | 2,072 |
| 54 | [htTweets@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=htTweets@mstdn.foxfam.club) | 2,028 |
| 55 | [cnbc@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=cnbc@mstdn.foxfam.club) | 2,024 |
| 56 | [algor@my.dirtyhobby.xyz](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=algor@my.dirtyhobby.xyz) | 2,006 |
| 57 | [Wn00Japan@pawoo.net](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=Wn00Japan@pawoo.net) | 1,968 |
| 58 | [rixty_dixet@squeet.me](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=rixty_dixet@squeet.me) | 1,931 |
| 59 | [natalie@chaosphere.hostdon.jp](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=natalie@chaosphere.hostdon.jp) | 1,889 |
| 60 | [zeit@mhc.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=zeit@mhc.social) | 1,876 |
| 61 | [deutschlandfunk@squeet.me](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=deutschlandfunk@squeet.me) | 1,846 |
| 62 | [DailyMail@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=DailyMail@mstdn.foxfam.club) | 1,820 |
| 63 | [techweb@pokemon.men](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=techweb@pokemon.men) | 1,801 |
| 64 | [NHK_NEWS@mastodon.chotto.moe](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=NHK_NEWS@mastodon.chotto.moe) | 1,759 |
| 65 | [ShitpostBot5000@twitter.1d4.us](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=ShitpostBot5000@twitter.1d4.us) | 1,750 |
| 66 | [arzachel@mastodon.derveni.org](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=arzachel@mastodon.derveni.org) | 1,731 |
| 67 | [misaka86@pawoo.net](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=misaka86@pawoo.net) | 1,730 |
| 68 | [trainchubu@mastodon.chotto.moe](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=trainchubu@mastodon.chotto.moe) | 1,669 |
| 69 | [zurlini@pawoo.net](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=zurlini@pawoo.net) | 1,667 |
| 70 | [2AWisdom@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=2AWisdom@mstdn.foxfam.club) | 1,662 |
| 71 | [kicker@friendica.produnis.de](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=kicker@friendica.produnis.de) | 1,645 |
| 72 | [nos@newsbots.eu](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=nos@newsbots.eu) | 1,642 |
| 73 | [aljazeera_english@newsbots.eu](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=aljazeera_english@newsbots.eu) | 1,642 |
| 74 | [ndtv@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=ndtv@mstdn.foxfam.club) | 1,631 |
| 75 | [cofebot@kartoffel.cafe](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=cofebot@kartoffel.cafe) | 1,626 |
| 76 | [todayilearned@botsin.space](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=todayilearned@botsin.space) | 1,618 |
| 77 | [RT@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=RT@mstdn.foxfam.club) | 1,598 |
| 78 | [losquesobran@friendica.chilemasto.casa](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=losquesobran@friendica.chilemasto.casa) | 1,590 |
| 79 | [esquerdadiario@newsbots.eu](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=esquerdadiario@newsbots.eu) | 1,588 |
| 80 | [bunposting@botsin.space](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=bunposting@botsin.space) | 1,585 |
| 81 | [vulnbot@schleuss.online](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=vulnbot@schleuss.online) | 1,578 |
| 82 | [elperiodico_cat@newsbots.eu](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=elperiodico_cat@newsbots.eu) | 1,571 |
| 83 | [plprensalatina@friendica.chilemasto.casa](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=plprensalatina@friendica.chilemasto.casa) | 1,562 |
| 84 | [apnews_bot@mastodon.cloud](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=apnews_bot@mastodon.cloud) | 1,556 |
| 85 | [dw@newsbots.eu](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=dw@newsbots.eu) | 1,546 |
| 86 | [killemall@mastodon.crazynewworld.net](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=killemall@mastodon.crazynewworld.net) | 1,541 |
| 87 | [trainkinki@mastodon.chotto.moe](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=trainkinki@mastodon.chotto.moe) | 1,537 |
| 88 | [thaitechfeed@botsin.space](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=thaitechfeed@botsin.space) | 1,533 |
| 89 | [nippon1@pawoo.net](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=nippon1@pawoo.net) | 1,531 |
| 90 | [RT@mastodon.internot.no](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=RT@mastodon.internot.no) | 1,520 |
| 91 | [scmpNews@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=scmpNews@mstdn.foxfam.club) | 1,520 |
| 92 | [sentinel@rebel.iero.org](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=sentinel@rebel.iero.org) | 1,498 |
| 93 | [army@propulse.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=army@propulse.club) | 1,485 |
| 94 | [indianexpress@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=indianexpress@mstdn.foxfam.club) | 1,484 |
| 95 | [the_hindu@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=the_hindu@mstdn.foxfam.club) | 1,475 |
| 96 | [comicbot@blob.cat](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=comicbot@blob.cat) | 1,466 |
| 97 | [ResumenLatinoamericano@newsbots.eu](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=ResumenLatinoamericano@newsbots.eu) | 1,465 |
| 98 | [uchileradio@friendica.chilemasto.casa](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=uchileradio@friendica.chilemasto.casa) | 1,457 |
| 99 | [srfnewsrss@social.pmj.rocks](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=srfnewsrss@social.pmj.rocks) | 1,454 |
| 100 | [rbnett@mastodon.internot.no](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=rbnett@mastodon.internot.no) | 1,444 |


[//]: # (do not edit this line end)


### Top 100 - Shared Cloudflare links
- [View latest information](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=c)
  - `Who`: Mastodon user
  - `Toots`: tweet has cloudflare link

[//]: # (do not edit this line start; t2)

| # | Who | Toots |
| --- | --- | --- |
| 1 | [amb_noticias@botsin.space](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=amb_noticias@botsin.space) | 9,223 |
| 2 | [tomohiro_amami@pawoo.net](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=tomohiro_amami@pawoo.net) | 3,330 |
| 3 | [covid_stats@mastodon.cloud](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=covid_stats@mastodon.cloud) | 3,002 |
| 4 | [elpuntavui@mastodont.cat](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=elpuntavui@mastodont.cat) | 2,893 |
| 5 | [RichardCharles@brighteon.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=RichardCharles@brighteon.social) | 2,413 |
| 6 | [prensa_maldita@friendica.chilemasto.casa](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=prensa_maldita@friendica.chilemasto.casa) | 2,375 |
| 7 | [hn50@social.lansky.name](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=hn50@social.lansky.name) | 2,126 |
| 8 | [thecliniccl@friendica.chilemasto.casa](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=thecliniccl@friendica.chilemasto.casa) | 2,094 |
| 9 | [hknewsbot@pawoo.net](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=hknewsbot@pawoo.net) | 1,774 |
| 10 | [misaka86@pawoo.net](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=misaka86@pawoo.net) | 1,730 |
| 11 | [cofebot@kartoffel.cafe](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=cofebot@kartoffel.cafe) | 1,626 |
| 12 | [itnewsbot@schleuss.online](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=itnewsbot@schleuss.online) | 1,503 |
| 13 | [hn100@social.lansky.name](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=hn100@social.lansky.name) | 1,417 |
| 14 | [kicker@friendica.produnis.de](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=kicker@friendica.produnis.de) | 1,352 |
| 15 | [johndolph@brighteon.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=johndolph@brighteon.social) | 1,318 |
| 16 | [scmpNews@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=scmpNews@mstdn.foxfam.club) | 1,245 |
| 17 | [dfghjk@pawoo.net](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=dfghjk@pawoo.net) | 1,154 |
| 18 | [hn100@botsin.space](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=hn100@botsin.space) | 1,151 |
| 19 | [el_ciudadano@friendica.chilemasto.casa](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=el_ciudadano@friendica.chilemasto.casa) | 1,110 |
| 20 | [army@propulse.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=army@propulse.club) | 1,093 |
| 21 | [elmostrador@friendica.chilemasto.casa](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=elmostrador@friendica.chilemasto.casa) | 1,062 |
| 22 | [elpuntavui@libranet.de](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=elpuntavui@libranet.de) | 995 |
| 23 | [teruteru128@pawoo.net](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=teruteru128@pawoo.net) | 987 |
| 24 | [epochtimes@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=epochtimes@mstdn.foxfam.club) | 976 |
| 25 | [hackernews@die-partei.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=hackernews@die-partei.social) | 956 |
| 26 | [esquerdadiario@newsbots.eu](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=esquerdadiario@newsbots.eu) | 948 |
| 27 | [alternet@newsbots.eu](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=alternet@newsbots.eu) | 890 |
| 28 | [SweetPony@equestria.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=SweetPony@equestria.social) | 886 |
| 29 | [PonyPics@equestria.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=PonyPics@equestria.social) | 885 |
| 30 | [Stripey@botsin.space](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=Stripey@botsin.space) | 865 |
| 31 | [hntooter@mastodon.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=hntooter@mastodon.social) | 858 |
| 32 | [Forbes@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=Forbes@mstdn.foxfam.club) | 829 |
| 33 | [GradientBot@botsin.space](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=GradientBot@botsin.space) | 823 |
| 34 | [thaitechfeed@botsin.space](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=thaitechfeed@botsin.space) | 816 |
| 35 | [FinancialTimes@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=FinancialTimes@mstdn.foxfam.club) | 802 |
| 36 | [NaturalNews@brighteon.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=NaturalNews@brighteon.social) | 778 |
| 37 | [washtimes@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=washtimes@mstdn.foxfam.club) | 766 |
| 38 | [Kachelmannwettr@fulda.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=Kachelmannwettr@fulda.social) | 733 |
| 39 | [hnbot@botsin.space](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=hnbot@botsin.space) | 696 |
| 40 | [Mfirebrand1@brighteon.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=Mfirebrand1@brighteon.social) | 693 |
| 41 | [arzachel@mastodon.derveni.org](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=arzachel@mastodon.derveni.org) | 675 |
| 42 | [ithome@hello.2heng.xin](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=ithome@hello.2heng.xin) | 658 |
| 43 | [axios@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=axios@mstdn.foxfam.club) | 639 |
| 44 | [androidpolice@mstdn.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=androidpolice@mstdn.social) | 622 |
| 45 | [politico@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=politico@mstdn.foxfam.club) | 612 |
| 46 | [meduzaproject@phreedom.tk](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=meduzaproject@phreedom.tk) | 595 |
| 47 | [HunDriverWidow@social.quodverum.com](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=HunDriverWidow@social.quodverum.com) | 579 |
| 48 | [hwnewsnetwork@mastodon.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=hwnewsnetwork@mastodon.social) | 569 |
| 49 | [blendernation@botsin.space](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=blendernation@botsin.space) | 549 |
| 50 | [MonDiaribot@mastodont.cat](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=MonDiaribot@mastodont.cat) | 548 |
| 51 | [DailyCaller@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=DailyCaller@mstdn.foxfam.club) | 539 |
| 52 | [tilderadio@tilde.zone](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=tilderadio@tilde.zone) | 519 |
| 53 | [hn250@social.lansky.name](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=hn250@social.lansky.name) | 518 |
| 54 | [resumen@friendica.chilemasto.casa](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=resumen@friendica.chilemasto.casa) | 493 |
| 55 | [VG@mastodon.internot.no](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=VG@mastodon.internot.no) | 490 |
| 56 | [gironanoticies@mastodon.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=gironanoticies@mastodon.social) | 483 |
| 57 | [TPostMillennial@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=TPostMillennial@mstdn.foxfam.club) | 479 |
| 58 | [luisarias371@libertalia.world](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=luisarias371@libertalia.world) | 475 |
| 59 | [news_ntd@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=news_ntd@mstdn.foxfam.club) | 475 |
| 60 | [shietka@mastodon.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=shietka@mastodon.social) | 457 |
| 61 | [mediazona@phreedom.tk](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=mediazona@phreedom.tk) | 442 |
| 62 | [tugatech@mastodon.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=tugatech@mastodon.social) | 428 |
| 63 | [broadbandforum@mstdn.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=broadbandforum@mstdn.social) | 425 |
| 64 | [ogatarin@pawoo.net](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=ogatarin@pawoo.net) | 403 |
| 65 | [Sanfermin@mastodon.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=Sanfermin@mastodon.social) | 402 |
| 66 | [NationCymru@toot.wales](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=NationCymru@toot.wales) | 402 |
| 67 | [precuresakura@pawoo.net](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=precuresakura@pawoo.net) | 393 |
| 68 | [enigmatico@fedi.absturztau.be](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=enigmatico@fedi.absturztau.be) | 390 |
| 69 | [thepigeonexpress@mastodon.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=thepigeonexpress@mastodon.social) | 374 |
| 70 | [sourcreampringles_SoundCloud@botsin.space](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=sourcreampringles_SoundCloud@botsin.space) | 374 |
| 71 | [FT@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=FT@mstdn.foxfam.club) | 372 |
| 72 | [jouwatch@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=jouwatch@mstdn.foxfam.club) | 359 |
| 73 | [cbcnews@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=cbcnews@mstdn.foxfam.club) | 357 |
| 74 | [gre@wxw.moe](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=gre@wxw.moe) | 356 |
| 75 | [WayneDupreeShow@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=WayneDupreeShow@mstdn.foxfam.club) | 355 |
| 76 | [Wn00Japan@pawoo.net](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=Wn00Japan@pawoo.net) | 348 |
| 77 | [AlexJones@brighteon.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=AlexJones@brighteon.social) | 347 |
| 78 | [mimi@mhc.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=mimi@mhc.social) | 346 |
| 79 | [Jleimer@brighteon.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=Jleimer@brighteon.social) | 345 |
| 80 | [IndiasMuslims@mastodon.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=IndiasMuslims@mastodon.social) | 329 |
| 81 | [EpochTimes@twitiverse.com](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=EpochTimes@twitiverse.com) | 322 |
| 82 | [gatewaypundit@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=gatewaypundit@mstdn.foxfam.club) | 320 |
| 83 | [NRO@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=NRO@mstdn.foxfam.club) | 313 |
| 84 | [Toni@brighteon.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=Toni@brighteon.social) | 310 |
| 85 | [CNN@societal.co](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=CNN@societal.co) | 310 |
| 86 | [SwarajyaMag@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=SwarajyaMag@mstdn.foxfam.club) | 309 |
| 87 | [todayilearned@botsin.space](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=todayilearned@botsin.space) | 306 |
| 88 | [artbot@botsin.space](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=artbot@botsin.space) | 304 |
| 89 | [CNN_politics_feed@noc.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=CNN_politics_feed@noc.social) | 299 |
| 90 | [itsecbot@schleuss.online](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=itsecbot@schleuss.online) | 295 |
| 91 | [appleinsider@mstdn.foxfam.club](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=appleinsider@mstdn.foxfam.club) | 288 |
| 92 | [barchbot@botsin.space](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=barchbot@botsin.space) | 286 |
| 93 | [Blenderdomain@botsin.space](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=Blenderdomain@botsin.space) | 276 |
| 94 | [DailyCaller@twitiverse.com](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=DailyCaller@twitiverse.com) | 265 |
| 95 | [thenewoil@freeradical.zone](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=thenewoil@freeradical.zone) | 259 |
| 96 | [admin@mastodon.bida.im](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=admin@mastodon.bida.im) | 253 |
| 97 | [noagendashownoteslinks@noagendasocial.com](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=noagendashownoteslinks@noagendasocial.com) | 252 |
| 98 | [RenewableAu@aus.social](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=RenewableAu@aus.social) | 252 |
| 99 | [rixty_dixet@squeet.me](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=rixty_dixet@squeet.me) | 248 |
| 100 | [interlignes@newsbots.eu](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=interlignes@newsbots.eu) | 245 |


[//]: # (do not edit this line end)


### Top 100 - Shared FQDN
- [View latest information](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=f)
  - `Cloudflare`: is listed as cloudflare user
  - `Anti-Tor`: is listed as anti-tor user
  - `Found in toots`: linked from these tweets

[//]: # (do not edit this line start; t3)

| # | FQDN | [Cloudflare](../cloudflare_users/domains) | [Anti-Tor](../anti-tor_users/fqdn) OR CF | Found in toots |
| --- | --- | --- | --- | --- |
| 1 | twitter.com |   |   | 205,689 |
| 2 | joejoe.github.io |   |   | 132,239 |
| 3 | t.co |   |   | 44,350 |
| 4 | www.youtube.com |   |   | 24,061 |
| 5 | bit.ly |   |   | 23,418 |
| 6 | news.google.com |   |   | 22,985 |
| 7 | youtu.be |   |   | 21,661 |
| 8 | www.smzdm.com |   |   | 19,208 |
| 9 | trib.al |   |   | 17,193 |
| 10 | nitter.twitiverse.com |   |   | 16,255 |
| 11 | nitter.net |   | &#128680; | 14,431 |
| 12 | tinyurl.com | &#127785; | &#128680; | 13,584 |
| 13 | radio.reclaim.technology |   |   | 11,520 |
| 14 | www.pixiv.net | &#127785; | &#128680; | 11,083 |
| 15 | transit.yahoo.co.jp |   |   | 10,554 |
| 16 | nitter.snopyta.org |   |   | 10,419 |
| 17 | radio.unitoo.it |   |   | 9,015 |
| 18 | nitter.mastodont.cat |   |   | 8,203 |
| 19 | www.cnbeta.com |   |   | 7,612 |
| 20 | tweets.newsbots.eu |   |   | 7,394 |
| 21 | github.com |   |   | 7,335 |
| 22 | news.ycombinator.com | &#127785; | &#128680; | 6,625 |
| 23 | metro.co.uk |   |   | 6,584 |
| 24 | incestbest.com |   |   | 6,378 |
| 25 | ow.ly |   |   | 6,250 |
| 26 | ift.tt |   |   | 6,111 |
| 27 | reut.rs |   |   | 5,872 |
| 28 | indianexpress.com |   |   | 5,715 |
| 29 | shindanmaker.com |   |   | 5,678 |
| 30 | www.reddit.com |   |   | 5,624 |
| 31 | www.dw.com |   |   | 5,102 |
| 32 | earthbus.org |   |   | 4,716 |
| 33 | www3.nhk.or.jp |   |   | 4,406 |
| 34 | www.tagesschau.de |   |   | 4,385 |
| 35 | www.deutschlandfunk.de |   |   | 4,296 |
| 36 | www.elpuntavui.cat | &#127785; | &#128680; | 4,073 |
| 37 | www.heise.de |   |   | 3,961 |
| 38 | www.tribuneindia.com |   |   | 3,897 |
| 39 | www.spiegel.de |   |   | 3,704 |
| 40 | www.bbc.co.uk |   |   | 3,675 |
| 41 | redd.it |   |   | 3,632 |
| 42 | www.amazon.co.jp |   | &#128680; | 3,579 |
| 43 | nitter.13ad.de |   |   | 3,569 |
| 44 | www.patrikai.com |   |   | 3,530 |
| 45 | feedproxy.google.com |   |   | 3,499 |
| 46 | v.gd | &#127785; | &#128680; | 3,305 |
| 47 | news.yahoo.co.jp |   |   | 3,303 |
| 48 | covid.yanoagenda.com | &#127785; | &#128680; | 3,183 |
| 49 | www.lemonde.fr |   |   | 3,149 |
| 50 | www.aljazeera.com |   |   | 3,070 |
| 51 | www.bbc.com |   |   | 3,058 |
| 52 | en.wikipedia.org |   |   | 3,057 |
| 53 | weibo.com |   |   | 3,012 |
| 54 | www.dailynews.co.th |   |   | 2,872 |
| 55 | www.techradar.com |   |   | 2,865 |
| 56 | nitter.pussthecat.org |   |   | 2,836 |
| 57 | www.svt.se |   |   | 2,767 |
| 58 | dig.chouti.com |   |   | 2,675 |
| 59 | www.eldiario.es |   |   | 2,673 |
| 60 | on.wsj.com |   |   | 2,669 |
| 61 | gitcom.org |   |   | 2,629 |
| 62 | tryst.link |   | &#128680; | 2,605 |
| 63 | www.theverge.com |   | &#128680; | 2,572 |
| 64 | sptnkne.ws |   |   | 2,552 |
| 65 | pub.dev |   |   | 2,473 |
| 66 | www.theguardian.com |   |   | 2,448 |
| 67 | open.spotify.com |   |   | 2,440 |
| 68 | www.theclinic.cl | &#127785; | &#128680; | 2,287 |
| 69 | www.thegatewaypundit.com | &#127785; | &#128680; | 2,125 |
| 70 | washex.am |   |   | 2,111 |
| 71 | www.zeit.de |   |   | 2,105 |
| 72 | natalie.mu |   |   | 2,095 |
| 73 | www.theepochtimes.com | &#127785; | &#128680; | 2,079 |
| 74 | i.imgur.com | &#127785; | &#128680; | 2,060 |
| 75 | dirtyhobby.xyz |   |   | 2,006 |
| 76 | derpibooru.org | &#127785; | &#128680; | 1,990 |
| 77 | passport.weibo.com |   |   | 1,956 |
| 78 | nitter.42l.fr |   |   | 1,910 |
| 79 | www.nytimes.com | &#127785; | &#128680; | 1,904 |
| 80 | www.vtv.gob.ve |   |   | 1,889 |
| 81 | www.hindustantimes.com |   |   | 1,885 |
| 82 | www.arte.tv |   | &#128680; | 1,862 |
| 83 | on.rt.com |   |   | 1,843 |
| 84 | www.rt.com |   |   | 1,840 |
| 85 | t13.cl | &#127785; | &#128680; | 1,817 |
| 86 | arxiv.org |   |   | 1,760 |
| 87 | danbooru.donmai.us | &#127785; | &#128680; | 1,740 |
| 88 | cnb.cx |   |   | 1,740 |
| 89 | www.twitch.tv |   |   | 1,738 |
| 90 | nos.nl |   |   | 1,725 |
| 91 | www.a-legend.net |   | &#128680; | 1,724 |
| 92 | www.golem.de |   |   | 1,701 |
| 93 | www.mk-mode.com |   |   | 1,674 |
| 94 | nitter.cattube.org |   |   | 1,664 |
| 95 | www.elperiodico.cat |   |   | 1,652 |
| 96 | www.techweb.com.cn |   |   | 1,646 |
| 97 | www.liberation.fr |   |   | 1,588 |
| 98 | invidious.snopyta.org |   |   | 1,578 |
| 99 | bbs.archlinux.org |   |   | 1,563 |
| 100 | www.scmp.com | &#127785; | &#128680; | 1,552 |


[//]: # (do not edit this line end)


### About Mastodon Users
- [View latest information](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=u)

[//]: # (do not edit this line start; t4)

| About | Users |
| --- | --- |
| Users who shared link | 24,017 |
| &#11169;  who shared ONLY Cloudflare link | 2,463 (10.26%) |
| &#11169;  who NEVER shared Cloudflare link | 13,726 (57.15%) |
| &#11169;  who shared BOTH links | 7,828 (32.59%) |
| &#127785;  **Mastodon Users who shared Cloudflare link** | **10,291 (42.85%)** |


[//]: # (do not edit this line end)


### About Mastodon Servers
- [View latest information](http://stopcloudflare@avrynpc2q7pknqa3ucf5tvjcwad5nxvxgwnzvl2b6dx6uo4f7nc7zzqd.onion/mastodon/?who=s)
  - `Cloudflare`: is listed as cloudflare user
  - `All Toots`: tweets which has any links sent from this server
  - `CF Toots`: tweets which has cloudflare links sent from this server

[//]: # (do not edit this line start; t5)

| # | Server | [Cloudflare](../cloudflare_users/domains) | All Toots | CF Toots | CF % |
| --- | --- | --- | --- | --- | --- |
| 1 | mstdn.foxfam.club |   | 130,214 | 16,229 | 12.46 |
| 2 | pawoo.net |   | 69,443 | 16,028 | 23.08 |
| 3 | botsin.space |   | 59,334 | 16,985 | 28.63 |
| 4 | mastodon.social | &#127785; | 54,185 | 9,797 | 18.08 |
| 5 | newsbots.eu |   | 52,349 | 3,281 | 6.27 |
| 6 | societal.co | &#127785; | 34,159 | 887 | 2.6 |
| 7 | twitiverse.com |   | 32,017 | 3,924 | 12.26 |
| 8 | friendica.chilemasto.casa |   | 29,152 | 7,505 | 25.74 |
| 9 | dajiaweibo.com |   | 24,362 | 13 | 0.05 |
| 10 | mamot.fr |   | 22,924 | 2,699 | 11.77 |
| 11 | pokemon.men | &#127785; | 13,546 | 0 | 0 |
| 12 | squeet.me |   | 13,018 | 406 | 3.12 |
| 13 | mastodon.chotto.moe |   | 12,439 | 0 | 0 |
| 14 | brighteon.social |   | 11,593 | 7,706 | 66.47 |
| 15 | toot-lab.reclaim.technology |   | 11,525 | 6 | 0.05 |
| 16 | mastodon.cloud | &#127785; | 10,417 | 3,141 | 30.15 |
| 17 | social.quodverum.com |   | 10,178 | 2,512 | 24.68 |
| 18 | mastodont.cat |   | 9,493 | 3,997 | 42.1 |
| 19 | framapiaf.org |   | 9,260 | 920 | 9.94 |
| 20 | noagendasocial.com | &#127785; | 8,393 | 2,415 | 28.77 |
| 21 | my.dirtyhobby.xyz |   | 8,094 | 0 | 0 |
| 22 | loma.ml |   | 7,613 | 191 | 2.51 |
| 23 | mstdn.social |   | 7,584 | 1,725 | 22.75 |
| 24 | mstdn.jp | &#127785; | 7,532 | 613 | 8.14 |
| 25 | mhc.social |   | 7,458 | 447 | 5.99 |
| 26 | schleuss.online |   | 6,880 | 1,798 | 26.13 |
| 27 | friendica.produnis.de |   | 6,157 | 1,719 | 27.92 |
| 28 | switter.at |   | 5,304 | 830 | 15.65 |
| 29 | mstdn.maud.io | &#127785; | 5,168 | 571 | 11.05 |
| 30 | die-partei.social |   | 4,834 | 959 | 19.84 |
| 31 | chaos.social |   | 4,750 | 497 | 10.46 |
| 32 | chaosphere.hostdon.jp |   | 4,653 | 232 | 4.99 |
| 33 | social.lansky.name |   | 4,196 | 4,196 | 100 |
| 34 | mastodon.bida.im |   | 4,082 | 634 | 15.53 |
| 35 | masthead.social | &#127785; | 4,041 | 159 | 3.93 |
| 36 | fedibird.com | &#127785; | 3,728 | 253 | 6.79 |
| 37 | libranet.de |   | 3,719 | 1,368 | 36.78 |
| 38 | mastodon.online |   | 3,707 | 393 | 10.6 |
| 39 | pomdon.work |   | 3,623 | 81 | 2.24 |
| 40 | social.tchncs.de |   | 3,610 | 470 | 13.02 |
| 41 | mastodon.internot.no |   | 3,553 | 581 | 16.35 |
| 42 | mastodon.in.th |   | 3,546 | 389 | 10.97 |
| 43 | mastodon.crazynewworld.net | &#127785; | 3,421 | 373 | 10.9 |
| 44 | sinblr.com | &#127785; | 3,106 | 234 | 7.53 |
| 45 | phreedom.tk |   | 3,042 | 1,153 | 37.9 |
| 46 | best-friends.chat |   | 2,981 | 392 | 13.15 |
| 47 | equestria.social |   | 2,865 | 2,003 | 69.91 |
| 48 | hello.2heng.xin | &#127785; | 2,635 | 776 | 29.45 |
| 49 | social.beachcom.org |   | 2,622 | 428 | 16.32 |
| 50 | 9kb.me |   | 2,613 | 6 | 0.23 |
| 51 | qoto.org |   | 2,565 | 218 | 8.5 |
| 52 | todon.nl |   | 2,458 | 411 | 16.72 |
| 53 | mstdn.guru |   | 2,410 | 189 | 7.84 |
| 54 | social.yl.ms |   | 2,316 | 6 | 0.26 |
| 55 | mastodon.xyz |   | 2,211 | 235 | 10.63 |
| 56 | imastodon.net |   | 2,175 | 77 | 3.54 |
| 57 | opensocial.at |   | 2,160 | 34 | 1.57 |
| 58 | mas.to |   | 2,152 | 342 | 15.89 |
| 59 | twitter.1d4.us | &#127785; | 2,108 | 303 | 14.37 |
| 60 | fosstodon.org |   | 2,105 | 411 | 19.52 |
| 61 | mstdn.kemono-friends.info | &#127785; | 2,048 | 95 | 4.64 |
| 62 | misskey.io | &#127785; | 2,030 | 242 | 11.92 |
| 63 | sociale.network |   | 1,979 | 153 | 7.73 |
| 64 | shitposter.club | &#127785; | 1,931 | 469 | 24.29 |
| 65 | octodon.social | &#127785; | 1,782 | 410 | 23.01 |
| 66 | blob.cat |   | 1,762 | 217 | 12.32 |
| 67 | mastodon.derveni.org |   | 1,731 | 676 | 39.05 |
| 68 | mstdn.io | &#127785; | 1,710 | 243 | 14.21 |
| 69 | mstdn.beer |   | 1,669 | 71 | 4.25 |
| 70 | kartoffel.cafe |   | 1,652 | 1,628 | 98.55 |
| 71 | mastodon.sk |   | 1,621 | 0 | 0 |
| 72 | qiitadon.com |   | 1,613 | 116 | 7.19 |
| 73 | aus.social |   | 1,578 | 279 | 17.68 |
| 74 | propulse.club |   | 1,573 | 1,107 | 70.38 |
| 75 | soc.cattube.org |   | 1,553 | 96 | 6.18 |
| 76 | rebel.iero.org |   | 1,498 | 241 | 16.09 |
| 77 | social.mikutter.hachune.net |   | 1,468 | 0 | 0 |
| 78 | social.pmj.rocks |   | 1,465 | 0 | 0 |
| 79 | kolektiva.social |   | 1,460 | 0 | 0 |
| 80 | social.politicaconciencia.org |   | 1,457 | 0 | 0 |
| 81 | liberdon.com |   | 1,437 | 0 | 0 |
| 82 | mastodon.technology |   | 1,380 | 0 | 0 |
| 83 | mstdn.nere9.help |   | 1,335 | 0 | 0 |
| 84 | friendica.utzer.de |   | 1,330 | 0 | 0 |
| 85 | k-shiigi-136.masto.host |   | 1,303 | 0 | 0 |
| 86 | pullopen.xyz |   | 1,284 | 0 | 0 |
| 87 | mstdn.fr |   | 1,284 | 0 | 0 |
| 88 | aleph.land |   | 1,254 | 0 | 0 |
| 89 | g0v.social |   | 1,251 | 0 | 0 |
| 90 | freespeechextremist.com |   | 1,248 | 0 | 0 |
| 91 | friendica.feneas.org |   | 1,245 | 0 | 0 |
| 92 | vocalodon.net |   | 1,229 | 0 | 0 |
| 93 | libresilicon.com |   | 1,164 | 0 | 0 |
| 94 | baraag.net | &#127785; | 1,158 | 0 | 0 |
| 95 | fedi.absturztau.be |   | 1,149 | 0 | 0 |
| 96 | bitcoinhackers.org | &#127785; | 1,145 | 0 | 0 |
| 97 | mastodon.uno | &#127785; | 1,138 | 0 | 0 |
| 98 | spinster.xyz | &#127785; | 1,109 | 0 | 0 |
| 99 | social.isurf.ca |   | 1,067 | 0 | 0 |
| 100 | cybre.space | &#127785; | 1,047 | 0 | 0 |


[//]: # (do not edit this line end)

----

![](../image/mastodoncf.jpg)
